#TODO
##Design
- add another model (layer) to deal with exact positioning of elements?
- draw elements & connections at once or in phases? <- draw in phases
    + coordinate-based drawing & manipulation vs. preemptive arrangement <- need both: rearrange before drawing (constraints + optimization for connections)
##Element
- type: circle
- type: trapeze
##Connection
- connect elements next to each other
- connect elements on same level
- connect elements on different levels
- types
    + directionless
    + directed
    + bothway
- rearrange items if connections overlap too much 
##Position
- rearrange items on same level to fit conditions
- rearrange items on multiple levels to fit conditions
##Levels
- add auto-assignment of elements to empty/default level
- add auto-detection of empty levels (shrink)?
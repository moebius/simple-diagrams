package at.waltersdorfer.simpleDiagrams;

import at.waltersdorfer.simpleDiagrams.logic.ASCIIGenerator;
import at.waltersdorfer.simpleDiagrams.logic.FileParser;
import at.waltersdorfer.simpleDiagrams.model.Level;
import org.junit.Test;

import java.util.List;

public class PlayTestSimpleDiagrams {

    @Test
    public void testParseFileOnSample1() throws Exception {
        FileParser parser = new FileParser();
        List<Level> levels = parser.parseFile("test/resources/sample1");

        StringBuilder builder = new StringBuilder();
        levels.forEach(level -> builder.append(level).append("\n"));
        System.out.println(builder.toString());
    }

    @Test
    public void testCreateASCIIDiagramOfSample1() throws Exception {
        FileParser parser = new FileParser();
        List<Level> levels = parser.parseFile("test/resources/sample1");

        ASCIIGenerator generator = new ASCIIGenerator(levels);
        System.out.println(generator.createDiagram());
    }
}

package at.waltersdorfer.simpleDiagrams.logic;

import at.waltersdorfer.simpleDiagrams.model.Connection;
import at.waltersdorfer.simpleDiagrams.model.Element;
import at.waltersdorfer.simpleDiagrams.model.Level;
import at.waltersdorfer.simpleDiagrams.model.Position;
import at.waltersdorfer.simpleDiagrams.sugar.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import static at.waltersdorfer.simpleDiagrams.model.Element.Type;
import static at.waltersdorfer.simpleDiagrams.sugar.Pair.asPair;
import static java.lang.Integer.parseInt;
import static java.util.Arrays.stream;

public class FileParser {

    private static final String START_TOKEN = "@startcomponent";
    private static final String ELEMENTS_TOKEN = "#elements";
    private static final String CONNECTIONS_TOKEN = "#connections";
    private static final String POSITIONS_TOKEN = "#position";
    private static final String LEVELS_TOKEN = "#levels";
    private static final String END_TOKEN = "@endcomponent";
    private static final String SEPARATOR = ":";
    private static final String INNER_SEPARATOR = ",";
    private static final String CONNECTOR = " ";

    private static final Map<State, String> nextSection = new HashMap<>();

    static {
        nextSection.put(State.INITIAL, ELEMENTS_TOKEN);
        nextSection.put(State.ELEMENTS, CONNECTIONS_TOKEN);
        nextSection.put(State.CONNECTIONS, POSITIONS_TOKEN);
        nextSection.put(State.POSITIONS, LEVELS_TOKEN);
        nextSection.put(State.LEVELS, END_TOKEN);
    }

    private static final String DEFAULT_ORIENTATION = "left-to-right";

    private State state;
    private List<Element> elements;
    private List<Level> levels;

    public List<Level> parseFile(String filename) {
        state = State.INITIAL;
        elements = new ArrayList<>();
        levels = new ArrayList<>();
        return readFile(filename);
    }

    private List<Level> readFile(String filename) {
        Path file = Paths.get(filename);
        try (InputStream in = Files.newInputStream(file);
             BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line;
            while ((line = reader.readLine()) != null) {
                parseLine(line);
            }
        } catch (IOException e) {
            System.err.println(e);
        }

        if (this.state.equals(State.FINAL)) {
            return levels;
        }

        throw new IllegalStateException("Parser not in a valid state: " + this.state);
    }

    private void parseLine(String line) {
        if (state.equals(State.FINAL)) {
            return;
        }
        if (line == null || line.trim().isEmpty()) {
            return;
        }
        if (nextSection.get(state).equals(line)) {
            state = state.advance();
            return;
        }

        String[] arguments = line.split(SEPARATOR);
        switch (state) {
            case INITIAL:
                if (START_TOKEN.equals(line) || DEFAULT_ORIENTATION.equals(line)) {
                    return;
                }
                return;
            case ELEMENTS:
                elements.add(elementFromLine(arguments));
                return;
            case CONNECTIONS:
                addConnection(arguments);
                return;
            case POSITIONS:
                addPosition(arguments);
                return;
            case LEVELS:
                levels.add(new Level(parseInt(arguments[0]))
                        .place(stream(arguments[1].split(INNER_SEPARATOR)).map(this::find)));
                return;
        }
        this.state = State.ERROR;
    }

    private static Element elementFromLine(String[] arguments) {
        return new Element(
                parseInt(arguments[0]),
                Type.valueOf(arguments[1].toUpperCase()),
                arguments[2]);
    }

    private void addConnection(String[] arguments) {
        Connection.Type type = Connection.Type.valueOf(arguments[0].toUpperCase());

        applyToPairsFrom(arguments[1],  pair -> pair.getFirst().addRelation(pair.getSecond(), type));
    }

    private void addPosition(String[] arguments) {
        Position.Type type = Position.Type.valueOf(arguments[0].toUpperCase());

        applyToPairsFrom(arguments[1], pair -> pair.getFirst().addRelation(pair.getSecond(), type));
    }

    private void applyToPairsFrom(String argument, Consumer<Pair<Element, Element>> pairConsumer) {
        stream(argument.split(INNER_SEPARATOR))
                .map(idPair -> elementsFromIdPair().apply(idPair.split(CONNECTOR)))
                .forEach(pairConsumer);
    }

    private Function<String[], Pair<Element, Element>> elementsFromIdPair() {
        return pair -> asPair(find(pair[0]), find(pair[1]));
    }

    private Element find(String number) {
        return elements.stream().filter(element -> element.getNumber() == parseInt(number)).findFirst().get();
    }

    private enum State {
        INITIAL, ELEMENTS, CONNECTIONS, LEVELS, POSITIONS, FINAL, ERROR;

        public State advance() {
            switch (this) {
                case INITIAL:
                    return ELEMENTS;
                case ELEMENTS:
                    return CONNECTIONS;
                case CONNECTIONS:
                    return POSITIONS;
                case POSITIONS:
                    return LEVELS;
                case LEVELS:
                    return FINAL;
                case FINAL:
                case ERROR:
            }
            return ERROR;
        }
    }
}

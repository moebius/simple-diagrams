package at.waltersdorfer.simpleDiagrams.logic;

import at.waltersdorfer.simpleDiagrams.model.Element;
import at.waltersdorfer.simpleDiagrams.model.Level;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ASCIIGenerator {
    private List<Level> diagram;

    public ASCIIGenerator(List<Level> diagram) {
        this.diagram = diagram;
    }

    public String createDiagram() {

        StringBuilder drawnDiagram = new StringBuilder();

        diagram.stream().forEach(drawLevel(drawnDiagram));

        return drawnDiagram.toString();
    }

    private Consumer<? super Level> drawLevel(StringBuilder drawnDiagram) {
        return level -> {
            StringBuilder topLine = new StringBuilder();
            StringBuilder middle = new StringBuilder();
            StringBuilder bottomLine = new StringBuilder();

            level.getElements().forEach(drawElement(topLine, middle, bottomLine));
            drawnDiagram.append(topLine).append('\n')
                    .append(middle).append('\n')
                    .append(bottomLine).append('\n')
                    .append('\n');
        };
    }

    private Consumer<? super Element> drawElement(StringBuilder topLine, StringBuilder middle, StringBuilder bottomLine) {
        return element -> {
            String name = element.getName();
            int length = name.length();
            char[] border = createCharacters('-', length + 2);

            topLine.append(border).append(' ');
            middle.append('|').append(name).append('|').append(' ');
            bottomLine.append(border).append(' ');
        };
    }

    private static char[] createCharacters(char value, int length) {
        char[] line = new char[length];
        Arrays.fill(line, value);
        return line;
    }
}

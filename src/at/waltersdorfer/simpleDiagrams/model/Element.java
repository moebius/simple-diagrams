package at.waltersdorfer.simpleDiagrams.model;

import java.util.HashSet;
import java.util.Set;

import static at.waltersdorfer.simpleDiagrams.sugar.Sugar.not;
import static java.util.Arrays.stream;

public class Element {
    private int number;
    private Type type;
    private String name;

    private Set<Connection> connections;
    private Level level;
    private Set<Position> positions;

    public Element(int number, Type type, String name) {
        this.number = number;
        this.type = type;
        this.name = name;
        connections = new HashSet<>();
        positions = new HashSet<>();
    }

    public void addRelation(Element other, Connection.Type type) {
        Connection connection = new Connection(this, other, type);
        this.connections.add(connection);
        if (not(connection.equals(Connection.Type.DIRECTED))) {
            other.getConnections().add(new Connection(other, this, type));
        }
    }

    public void addRelation(Element other, Position.Type type) {
        Position position = new Position(this, other, type);
        this.positions.add(position);

        Position invertedPosition = new Position(this, other, type.invert());
        other.getPositions().add(invertedPosition);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Connection> getConnections() {
        return connections;
    }

    public void setConnections(Set<Connection> connections) {
        this.connections = connections;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Set<Position> getPositions() {
        return positions;
    }

    public void setPositions(Set<Position> positions) {
        this.positions = positions;
    }

    public enum Type {
        BOX, CIRCLE, TRAPEZE
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("id: ").append(number).append("\n");
        builder.append("type: ").append(type).append("\n");
        builder.append("name: ").append(name).append("\n");

        if (positions != null && not(positions.isEmpty())) {
            builder.append("positions:\n");
            positions.forEach(position ->
                    builder.append("\t").append(position.getType())
                            .append(" of ")
                            .append(position.getSecond().getNumber()).append(", "));
            builder.append("\n");
        }

        if (connections != null && not(connections.isEmpty())) {
            builder.append("connections:\n");
            connections.forEach(connection ->
                    builder.append(number)
                            .append(connection.getType().getDescription())
                            .append(connection.getSecond().getNumber()).append(", "));
        }

        return builder.toString();
    }
}

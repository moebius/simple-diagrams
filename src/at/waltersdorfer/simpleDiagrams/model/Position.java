package at.waltersdorfer.simpleDiagrams.model;

import java.util.stream.Stream;

import static java.util.Arrays.stream;

public class Position {

    private Type type;
    private Element first;
    private Element second;

    public Position(Element first, Element second, Type type) {
        this.first = first;
        this.second = second;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Element getFirst() {
        return first;
    }

    public void setFirst(Element first) {
        this.first = first;
    }

    public Element getSecond() {
        return second;
    }

    public void setSecond(Element second) {
        this.second = second;
    }

    public enum Type {
        LEFT, RIGHT;

        public Type invert() {
            if (this.equals(LEFT)) {
                return RIGHT;
            } else {
                return LEFT;
            }
        }

        public static Stream<String> getValues() {
            return stream(values()).map(Enum::name);
        }
    }
}

package at.waltersdorfer.simpleDiagrams.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static at.waltersdorfer.simpleDiagrams.sugar.Sugar.not;

public class Level {
    private int number;
    private List<Element> elements;

    public Level(int number) {
        this.number = number;
        this.elements = new ArrayList<>();
    }

    public Level place(Stream<Element> elements) {
        elements.forEach(element -> {
            element.setLevel(this);
            this.elements.add(element);
        });
        return this;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Level ").append(number).append(":\n");

        if (elements != null && not(elements.isEmpty())) {
            elements.forEach(builder::append);
        }

        return builder.toString();
    }
}

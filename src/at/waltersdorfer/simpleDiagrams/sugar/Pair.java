package at.waltersdorfer.simpleDiagrams.sugar;

public class Pair<K, V> {
    protected K key;
    protected V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getFirst(){
        return key;
    }

    public V getSecond(){
        return value;
    }

    public static <K, V> Pair<K, V> asPair(K key, V value) {
        return new Pair<>(key, value);
    }
}
